import React, { useContext } from "react";
import { Route, Redirect } from "react-router-dom";
import ScreenLoader from "../components/Skeleton/ScreenLoader";
import { LoginContext } from "../context/loginContext";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const [state] = useContext(LoginContext);

  return (
    <div>
      <Route
        {...rest}
        render={(props) =>
          state.loading ? (
            <ScreenLoader />
          ) : state.isLogin ? (
            state.user.isAdmin === "0" ? (
              <Component {...props} />
            ) : (
              <Redirect to="/admin" />
            )
          ) : (
            <Redirect to="/" />
          )
        }
      />
    </div>
  );
};

export default PrivateRoute;
