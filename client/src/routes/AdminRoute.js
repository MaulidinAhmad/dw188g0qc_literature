import React, { useContext } from "react";
import { Route, Redirect } from "react-router-dom";
import ScreenLoader from "../components/Skeleton/ScreenLoader";
import { LoginContext } from "../context/loginContext";

const AdminRoute = ({ component: Component, ...rest }) => {
  const [state] = useContext(LoginContext);

  return (
    <div>
      <Route
        {...rest}
        render={(props) =>
          state.loading ? (
            <ScreenLoader />
          ) : state.isLogin ? (
            state.user.isAdmin === "1" ? (
              <Component {...props} />
            ) : (
              <Redirect to="/index" />
            )
          ) : (
            <Redirect to="/" />
          )
        }
      />
    </div>
  );
};

export default AdminRoute;
