import React from "react";
import { Route, Switch } from "react-router-dom";
import AfterLogin from "../components/Skeleton/AfterLogin";
import AddBook from "../views/AddBook";
import Detail from "../views/Detail";
import Index from "../views/Index";
import Landing from "../views/Landing";
import Collection from "../views/MyCollection";
import Profile from "../views/Profile";
// import ReadBook from "../views/ReadBook";
import PrivateRoute from "./PrivateRoute";
import adminIndex from "../views/admin/Index";
import adminAdd from "../views/admin/Add";
// import AfterLogin from "../components/Skeleton/AfterLogin";
// import NotFound404 from "../views/errors/NotFound404";
import AdminRoute from "./AdminRoute";

function Routes(props) {
  return (
    <Switch>
      <Route exact path="/" component={Landing} />

      <AdminRoute path="/admin" exact component={adminIndex} />
      <AdminRoute path="/admin/add" component={adminAdd} />
      <Route
        exact
        path={["/index", "/detail/:id", "/add", "/profile", "/collection"]}
      >
        <AfterLogin>
          <Switch>
            <PrivateRoute exact path="/index" component={Index} />
            <PrivateRoute exact path="/collection" component={Collection} />
            <PrivateRoute exact path="/detail/:id" component={Detail} />
            <PrivateRoute exact path="/add" component={AddBook} />
            <PrivateRoute exact path="/profile" component={Profile} />
          </Switch>
        </AfterLogin>
      </Route>

      {/* <Route path="*" component={NotFound404} />  */}
    </Switch>
  );
}

export default Routes;
