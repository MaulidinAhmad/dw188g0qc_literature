import { Transition } from "@tailwindui/react";
import React, {
  useEffect,
  useState,
  useRef,
  useCallback,
  useContext,
} from "react";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import { API } from "../../config/api";
import { LoginContext } from "../../context/loginContext";

// Increase pixel density for crop preview quality on retina screens.
const pixelRatio = window.devicePixelRatio || 1;

function ProfilePictureModal(props) {
  const [upImg, setUpImg] = useState();
  const imgRef = useRef(null);
  const [fileName, setfileName] = useState();
  // eslint-disable-next-line
  const [state, dispatch] = useContext(LoginContext);
  // const previewCanvasRef = useRef(null);
  const [crop, setCrop] = useState({
    unit: "%",
    width: 90,
    aspect: 1,
    x: 25,
    y: 25,
  });
  const [completedCrop, setCompletedCrop] = useState(null);

  // On Image Selected
  const onSelectFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
      setfileName(e.target.files[0].name);
    }
  };

  const onSubmitImage = async () => {
    const img = imgRef.current;
    const crop = completedCrop;
    const canvas = document.createElement("canvas");
    // canvas.width = crop.width;
    // canvas.height = crop.height;
    const scaleX = img.naturalWidth / img.width;
    const scaleY = img.naturalHeight / img.height;
    const ctx = canvas.getContext("2d");

    canvas.width = crop.width * pixelRatio;
    canvas.height = crop.height * pixelRatio;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = "high";

    ctx.drawImage(
      img,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );

    // console.log(fileName);
    const Blob = new Promise((resolve, reject) => {
      canvas.toBlob(
        (blob) => {
          blob.name = fileName;
          blob.lastModifiedDate = new Date();
          resolve(blob);
        },
        "image/jpeg",
        1
      );
    });

    const newBlob = await Blob;

    // console.log(newBlob);

    const newImage = new File([newBlob], fileName, { type: "image/jpeg" });
    // console.log(newImage);
    const data = new FormData();
    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };
    data.append("profileImg", newImage);
    const res = await API.post("/user/uploadimage", data, config);

    dispatch({
      type: "UPDATE_USER",
      payload: res.data.data,
    });
    props.handleProfile();
  };

  const onLoad = useCallback((img) => {
    imgRef.current = img;
  }, []);

  useEffect(() => {
    if (!completedCrop || !imgRef.current) {
      return;
    }
  }, [completedCrop]);

  useEffect(() => {
    props.show
      ? (document.body.style.overflow = "hidden")
      : (document.body.style.overflow = "auto");
  }, [props.show]);

  return (
    <>
      <Transition
        show={props.show}
        enter="transition duration-100"
        enterFrom="opacity-0 scale-95"
        enterTo="opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="opacity-100 scale-100"
        leaveTo="opacity-0 scale-95"
      >
        <div className="fixed z-10 inset-0 overflow-y-auto">
          <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div
              className="fixed inset-0 transition-opacity"
              onClick={() => props.handleProfile()}
            >
              <div className="absolute inset-0 bg-gray-300 opacity-50"></div>
            </div>
            <span className="hidden sm:inline-block sm:align-middle sm:h-screen"></span>

            <div
              className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
              role="dialog"
              aria-modal="true"
              aria-labelledby="modal-headline"
            >
              <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <div className="sm:flex sm:items-center justify-center text-center">
                  <div className="mt-3 sm:mt-0 sm:ml-4 sm:text-center">
                    <h3
                      className="text-lg leading-6 font-medium text-gray-900"
                      id="modal-headline"
                    >
                      Change Profile Picture
                    </h3>
                    <div className="mt-2">
                      <div>
                        <input
                          id="profile-input"
                          type="file"
                          className="hidden"
                          accept="image/*"
                          onChange={onSelectFile}
                        />
                      </div>
                      <div className="block">
                        <ReactCrop
                          imageStyle={{ maxHeight: "400px" }}
                          locked
                          src={upImg}
                          onImageLoaded={onLoad}
                          crop={crop}
                          onChange={(c) => setCrop(c)}
                          onComplete={(c) => setCompletedCrop(c)}
                        />
                      </div>
                      <button
                        className="px-2 py-2 bg-gray-300 focus:outline-none rounded-md text-black mr-3"
                        onClick={() => {
                          document.getElementById("profile-input").click();
                        }}
                      >
                        Choose Image
                      </button>
                      <button
                        onClick={() => onSubmitImage()}
                        className="px-2 py-2 bg-orange-custom focus:outline-none rounded-md text-white"
                      >
                        Submit
                      </button>
                      {/* <button
                        type="button"
                        disabled={
                          !completedCrop?.width || !completedCrop?.height
                        }
                        onClick={() =>
                          generateDownload(
                            previewCanvasRef.current,
                            completedCrop
                          )
                        }
                      >
                        Download cropped image
                      </button> */}
                      {/* <p className="text-sm leading-5 text-gray-500">
                        Are you sure you want to deactivate your account? All of
                        your data will be permanently removed. This action
                        cannot be undone.
                      </p> */}
                    </div>
                  </div>
                </div>
              </div>
              <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                <span className="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                  <button
                    onClick={() => props.handleProfile()}
                    type="button"
                    className="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-red transition ease-in-out duration-150 sm:text-sm sm:leading-5"
                  >
                    Cancel
                  </button>
                </span>
              </div>
            </div>
          </div>
        </div>
      </Transition>
    </>
  );
}

export default ProfilePictureModal;
