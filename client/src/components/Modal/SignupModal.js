import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import { LoginContext } from "../../context/loginContext";
import { API, setAuthToken, headerConfig } from "../../config/api";
import LoaderButton from "../Skeleton/LoaderButton";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

export default function SignupModal(props) {
  // eslint-disable-next-line
  const [state, dispatch] = useContext(LoginContext);
  const [loading, setloading] = useState(false);
  const [emailUsed, setemailUsed] = useState(false);

  const initialValues = {
    email: "",
    password: "",
    fullName: "",
    gender: "",
    phone: "",
    address: "",
  };

  const Schema = Yup.object().shape({
    email: Yup.string().email().required("Email Not Allow To be Empty"),
    password: Yup.string().required("Password Not Allow To be Empty"),
    fullName: Yup.string().required("Full Name Not Allow To be Empty"),
    gender: Yup.string().required("Gender Not Allow To be Empty"),
    phone: Yup.number().required("Phone Not Allow To be Empty"),
    address: Yup.string().required("Address Not Allow To be Empty"),
  });

  const history = useHistory();

  const handleSubmit = async (e, action) => {
    setloading(true);
    const body = JSON.stringify(e);
    try {
      const res = await API.post("/register", body, headerConfig);
      dispatch({
        type: "LOGIN_SUCCESS",
        payload: res.data.data,
      });
      setAuthToken(res.data.data.token);
      setemailUsed(false);
      setloading(false);
      try {
        const res = await API.get("/auth");
        dispatch({
          type: "USER_LOADED",
          payload: res.data.data.user,
        });
        history.push("/index");
      } catch (err) {
        dispatch({
          type: "AUTH_ERROR",
        });
        setloading(false);
      }
    } catch (err) {
      dispatch({
        type: "LOGIN_FAIL",
      });
      setemailUsed(true);
      setloading(false);
    }
  };
  return (
    <>
      {props.toggle ? (
        <>
          <Formik
            initialValues={initialValues}
            validationSchema={Schema}
            onSubmit={(e, action) => {
              handleSubmit(e, action);
            }}
          >
            {(formik) => {
              const { errors, touched } = formik;
              return (
                <Form>
                  <div className="justify-center items-center flex overflow-x-hidden fixed inset-0 z-50 outline-none focus:outline-none">
                    <div
                      className="relative xs:w-auto my-6 mx-auto max-w-3xl"
                      style={{ width: "416px" }}
                    >
                      {/*content*/}
                      <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-black outline-none focus:outline-none">
                        {/*header*/}
                        <div className="flex items-start justify-between p-5  rounded-t">
                          <h3 className="text-3xl text-white font-semibold">
                            Sign Up
                          </h3>
                          <button
                            className="p-1 ml-auto bg-transparent border-0 text-white opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                            onClick={() => props.toggleHandler()}
                          >
                            <span className="bg-transparent text-white opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                              ×
                            </span>
                          </button>
                        </div>
                        {/*body*/}
                        <div className="relative px-10 flex-auto">
                          <div className="w-full max-w-sm ">
                            <div className="md:flex md:items-center mb-6">
                              <div className="md:w-full">
                                <Field
                                  className={`bg-gray-600  ${
                                    errors.email && touched.email
                                      ? "border-red-500 focus:border-red-500"
                                      : "border-gray-200 focus:border-white"
                                  } appearance-none border-2 placeholder-white rounded-md w-full py-2 px-4 text-white  leading-tight focus:outline-none`}
                                  type="text"
                                  name="email"
                                  placeholder="Email"
                                />
                                <ErrorMessage
                                  name="email"
                                  component="p"
                                  className="text-red-500 text-xs italic"
                                />
                              </div>
                            </div>
                            <div className="md:flex md:items-center mb-6">
                              <div className="md:w-full">
                                <Field
                                  className={`bg-gray-600  ${
                                    errors.password && touched.password
                                      ? "border-red-500 focus:border-red-500"
                                      : "border-gray-200 focus:border-white"
                                  } appearance-none border-2 placeholder-white rounded-md w-full py-2 px-4 text-white  leading-tight focus:outline-none`}
                                  type="password"
                                  name="password"
                                  placeholder="Password"
                                />
                                <ErrorMessage
                                  name="password"
                                  component="p"
                                  className="text-red-500 text-xs italic"
                                />
                              </div>
                            </div>
                            <div className="md:flex md:items-center mb-6">
                              <div className="md:w-full">
                                <Field
                                  className={`bg-gray-600  ${
                                    errors.fullName && touched.fullName
                                      ? "border-red-500 focus:border-red-500"
                                      : "border-gray-200 focus:border-white"
                                  } appearance-none border-2 placeholder-white rounded-md w-full py-2 px-4 text-white  leading-tight focus:outline-none`}
                                  type="text"
                                  name="fullName"
                                  placeholder="Full Name"
                                />
                                <ErrorMessage
                                  name="fullName"
                                  component="p"
                                  className="text-red-500 text-xs italic"
                                />
                              </div>
                            </div>
                            <div className="md:flex md:items-center mb-6">
                              <div className="md:w-full">
                                <Field
                                  as="select"
                                  className={`bg-gray-600  ${
                                    errors.gender && touched.gender
                                      ? "border-red-500 focus:border-red-500"
                                      : "border-gray-200 focus:border-white"
                                  } appearance-none border-2 placeholder-white rounded-md w-full py-2 px-4 text-white  leading-tight focus:outline-none`}
                                  id="grid-state"
                                  name="gender"
                                >
                                  <option value="">Gender</option>
                                  <option value="male">Male</option>
                                  <option value="female">Female</option>
                                </Field>
                                <ErrorMessage
                                  name="gender"
                                  component="p"
                                  className="text-red-500 text-xs italic"
                                />
                              </div>
                            </div>
                            <div className="md:flex md:items-center mb-6">
                              <div className="md:w-full">
                                <Field
                                  className={`bg-gray-600  ${
                                    errors.phone && touched.phone
                                      ? "border-red-500 focus:border-red-500"
                                      : "border-gray-200 focus:border-white"
                                  } appearance-none border-2 placeholder-white rounded-md w-full py-2 px-4 text-white  leading-tight focus:outline-none`}
                                  type="text"
                                  name="phone"
                                  placeholder="Phone"
                                />
                                <ErrorMessage
                                  name="phone"
                                  component="p"
                                  className="text-red-500 text-xs italic"
                                />
                              </div>
                            </div>
                            <div className="md:flex md:items-center mb-6">
                              <div className="md:w-full">
                                <Field
                                  className={`bg-gray-600  ${
                                    errors.address && touched.address
                                      ? "border-red-500 focus:border-red-500"
                                      : "border-gray-200 focus:border-white"
                                  } appearance-none border-2 placeholder-white rounded-md w-full py-2 px-4 text-white  leading-tight focus:outline-none`}
                                  type="text"
                                  name="address"
                                  placeholder="Address"
                                />
                                <ErrorMessage
                                  name="address"
                                  component="p"
                                  className="text-red-500 text-xs italic"
                                />
                              </div>
                            </div>
                            <div className="block justify-center text-center italic">
                              {emailUsed ? (
                                <p className="text-red-500 mb-6">
                                  Your Email Has Been Taken
                                </p>
                              ) : (
                                ""
                              )}
                            </div>
                          </div>
                        </div>
                        {/*footer*/}
                        <div className="flex items-center w-full justify-center px-10 pb-4">
                          <button
                            className="bg-orange-custom text-white active:bg-green-600 w-full font-bold uppercase text-sm px-auto py-3 rounded-md shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                            type="submit"
                            style={{ transition: "all .15s ease" }}
                            disabled={loading}
                          >
                            {loading ? <LoaderButton /> : "Sign Up"}
                          </button>
                        </div>
                        <div className="flex items-center w-full justify-center px-10 pb-6  rounded-b">
                          <p></p>
                          <p>
                            Already have an account ? klik<span> </span>
                            <span
                              onClick={() => {
                                props.toggleHandler();
                                props.toggleLogin();
                              }}
                              href="/"
                              className="ml-1 cursor-pointer"
                            >
                              Here
                            </span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
          <div
            onClick={() => props.toggleHandler()}
            className="opacity-25 fixed inset-0 z-40 bg-black"
          ></div>
        </>
      ) : null}
    </>
  );
}
