import React, { useState } from "react";
import { BsSearch } from "react-icons/bs";

function Search(props) {
  const [query, setquery] = useState(props.initValue);

  const handleSearch = (e) => {
    e.preventDefault();
    if (props.active === false) {
      props.handleShowLogo();
    }
    props.handleInput(query);
  };

  const handleChange = (e) => {
    setquery(e.target.value);
  };
  return (
    <div className={`w-5/12 ${props.active ? "mt-0" : " mt-8"}`}>
      <form onSubmit={(e) => handleSearch(e)}>
        <div className="flex items-center">
          <input
            className={`bg-gray-600  
              appearance-none border-2 placeholder-white rounded w-full py-2 px-4 text-white  leading-tight focus:outline-none `}
            type="text"
            name="search"
            onChange={(e) => handleChange(e)}
            placeholder="Search for literature"
            value={query || ""}
          />

          <button
            type="submit"
            className="flex-shrink-0 px-3 rounded-md ml-3 focus:outline-none py-3 bg-orange-custom text-white"
          >
            <BsSearch />
          </button>
        </div>
      </form>
    </div>
  );
}

export default Search;
