import React from "react";
import { useQuery } from "react-query";
import { API } from "../config/api";

function SearchFilter(props) {
  const year = [];

  const {
    isLoading,
    error,
    data: rangeData,
    refetch,
  } = useQuery("getRangeData", () => API.get(`/getyearrange`));
  const handleChange = (e) => {
    props.handleInput(e.target.value);
  };
  // if (isLoading || !rangeData) {
  //   return null;
  // } else {
  //   for (let index = rangeData.minYear; index <= rangeData.maxYear; index++) {
  //     year.push(index);
  //   }
  // }

  return (
    <div className="flex-row">
      <p
        onClick={() => props.handleInput("")}
        className="text-orange-custom cursor-pointer"
      >
        Anytime
      </p>
      <select
        name="year"
        className="text-white border-white rounded-md focus:outline-none bg-gray-700"
        onChange={(e) => handleChange(e)}
        id=""
      >
        {isLoading || !rangeData ? (
          <option>Loading....</option>
        ) : (
          <>
            <option>Select Year</option>
            {/* {year.map((val, index) => ( */}
            <option value="2020">Since 2020</option>
            <option value="2019">Since 2019</option>
            <option value="2018">Since 2018</option>
            <option value="2017">Since 2017</option>
            <option value="2016">Since 2016</option>
            <option value="2015">Since 2015</option>
            {/* ))} */}
          </>
        )}
      </select>
    </div>
  );
}

export default SearchFilter;
