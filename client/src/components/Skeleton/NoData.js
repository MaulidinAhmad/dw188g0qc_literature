import React from "react";

function NoData(props) {
  return (
    <div className="bg-gray-400 px-3 py-3 rounded-md">
      <p className="text-xl text-center">Data Not Found</p>
    </div>
  );
}

export default NoData;
