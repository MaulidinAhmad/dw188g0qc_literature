import React from "react";

function ScreenLoader(props) {
  return (
    <div className="flex content-center h-screen flex-wrap">
      <div className="loader ease-linear rounded-full border-8 border-t-8 mx-auto border-gray-200 h-20 w-20"></div>
    </div>
  );
}

export default ScreenLoader;
