import React from "react";
import "../../assets/custom.css";
function LoaderButton(props) {
  return (
    <div>
      <div className="loader ease-linear rounded-full border-8 border-t-8 mx-auto border-gray-200 h-1 w-1"></div>
    </div>
  );
}

export default LoaderButton;
