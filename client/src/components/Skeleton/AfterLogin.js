import React, { useContext } from "react";
import { LoginContext } from "../../context/loginContext";
import { Redirect } from "react-router-dom";
import ScreenLoader from "./ScreenLoader";
import Navbar from "../Navbar";

function AfterLogin(props) {
  const [state] = useContext(LoginContext);
  return (
    <>
      {state.loading ? (
        <ScreenLoader />
      ) : state.isLogin ? (
        <div>
          <Navbar />
          <div className="grid grid-cols-12 content-center px-10">
            <div className="md:mb-8 col-span-12 mt-4">{props.children}</div>
          </div>
        </div>
      ) : (
        <Redirect path="/" />
      )}
    </>
  );
}

export default AfterLogin;
