import { Transition } from "@tailwindui/react";
import React from "react";

function IndexLogo(props) {
  return (
    <Transition
      show={props.show}
      enter="transition"
      enterFrom="opacity-0"
      enterTo="opacity-100"
      leave="transition"
      leaveFrom="opacity-100"
      leaveTo="opacity-0"
    >
      <div className="mt-16">
        <img src={require("../assets/image/search_logo.png")} alt="" />
      </div>
    </Transition>
  );
}

export default IndexLogo;
