import React, { useContext } from "react";
import { NavLink, useHistory } from "react-router-dom";
import { LoginContext } from "../context/loginContext";
import Logo from "./Logo";

function Navbar(props) {
  const [state, dispatch] = useContext(LoginContext);
  const history = useHistory();
  const handleLogout = () => {
    dispatch({
      type: "LOGOUT",
    });
    history.push("/");
  };

  return (
    <div>
      <div className="flex px-10 py-8">
        <NavLink to="/profile" className="mr-8 text-white">
          Profile
        </NavLink>
        <NavLink to="/collection" className="mr-8 text-white">
          My Collection
        </NavLink>
        <NavLink to="/add" className="mr-8 text-white">
          Add Literature
        </NavLink>
        <div
          onClick={() => handleLogout()}
          className="mr-8 cursor-pointer text-white"
        >
          Logout
        </div>

        <div className="ml-auto">
          <Logo />
        </div>
      </div>
    </div>
  );
}

export default Navbar;
