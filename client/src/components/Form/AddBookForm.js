import React, { useContext, useEffect, useState } from "react";
import { BiBookAdd } from "react-icons/bi";
// import CKEditor from "@ckeditor/ckeditor5-react";
// import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import Toast from "../Toast/Toast";
import { API } from "../../config/api";
import { LoginContext } from "../../context/loginContext";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

function AddBookForm(props) {
  const [show, setshow] = useState(false);
  const [loginState] = useContext(LoginContext);

  const initialValues = {
    title: "",
    publication: "",
    pages: "",
    isbn: "",
    about: "",
    author: "",
    file: null,
  };

  const Schema = Yup.object().shape({
    title: Yup.string().required("Title Not Allow To Empty"),
    publication: Yup.string().required("Publication Not Allow To Empty"),
    author: Yup.string().required("Author Not Allow To Empty"),
    pages: Yup.number().required("Pages Not Allow To Empty"),
    isbn: Yup.number().required("ISBN Not Allow To Empty"),
    file: Yup.mixed()
      .nullable("File Not Allow To Empty")
      .test(
        "fileFormat",
        "PDF only",
        (value) => value && ["application/pdf"].includes(value.type)
      ),
  });

  const handleSubmit = async (e, action) => {
    // console.log(action);

    try {
      const config = {
        headers: {
          "content-type": "multipart/form-data",
        },
      };
      let body = new FormData();

      if (!loginState.loading) {
        if (loginState.user.isAdmin === "1") {
          body.append("status", "Approved");
        }
      }
      body.append("title", e.title);
      body.append("publication", e.publication);
      body.append("author", e.author);
      body.append("pages", e.pages);
      body.append("ISBN", e.isbn);
      body.append("file", e.file);
      await API.post("/literature", body, config);
      action.resetForm(initialValues);
      setshow(!show);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <Formik
        initialValues={initialValues}
        validationSchema={Schema}
        onSubmit={(e, action) => {
          handleSubmit(e, action);
        }}
      >
        {(formik) => {
          const {
            errors,
            touched,
            isValid,
            dirty,
            setFieldValue,
            values,
          } = formik;
          return (
            <Form className="w-full max-w-full ">
              <div className="flex flex-wrap mb-6">
                <div className="w-full px-3">
                  <label
                    className="block uppercase tracking-wide text-white text-xs font-bold mb-2"
                    htmlFor="grid-title"
                  >
                    Title
                  </label>
                  <Field
                    className={`bg-gray-600  ${
                      errors.title && touched.title
                        ? "border-red-500 focus:border-red-500"
                        : "border-gray-200 focus:border-white"
                    } appearance-none border-2 rounded w-full py-2 px-4 text-white  leading-tight focus:outline-none placeholder-white `}
                    id="grid-title"
                    type="text"
                    placeholder="Title"
                    name="title"
                  />
                  <ErrorMessage
                    name="title"
                    component="p"
                    className="text-red-500 text-xs italic"
                  />
                </div>
              </div>
              <div className="flex flex-wrap mb-6">
                <div className="w-full px-3">
                  <label
                    className="block uppercase tracking-wide text-white text-xs font-bold mb-2"
                    htmlFor="grid-date"
                  >
                    Publication Date
                  </label>
                  <Field
                    className={`bg-gray-600  ${
                      errors.publication && touched.publication
                        ? "border-red-500 focus:border-red-500"
                        : "border-gray-200 focus:border-white"
                    } appearance-none border-2 rounded w-full py-2 px-4 text-white  leading-tight focus:outline-none placeholder-white `}
                    id="grid-date"
                    type="text"
                    placeholder="Publication Date"
                    name="publication"
                  />
                  <ErrorMessage
                    name="publication"
                    component="p"
                    className="text-red-500 text-xs italic"
                  />
                </div>
              </div>
              <div className="flex flex-wrap mb-6">
                <div className="w-full px-3">
                  <label
                    className="block uppercase tracking-wide text-white text-xs font-bold mb-2"
                    htmlFor="grid-pages"
                  >
                    Pages
                  </label>
                  <Field
                    className={`bg-gray-600  ${
                      errors.pages && touched.pages
                        ? "border-red-500 focus:border-red-500"
                        : "border-gray-200 focus:border-white"
                    } appearance-none border-2 rounded w-full py-2 px-4 text-white  leading-tight focus:outline-none placeholder-white `}
                    id="grid-pages"
                    type="number"
                    placeholder="Pages"
                    name="pages"
                  />
                  <ErrorMessage
                    name="pages"
                    component="p"
                    className="text-red-500 text-xs italic"
                  />
                </div>
              </div>
              <div className="flex flex-wrap mb-6">
                <div className="w-full px-3">
                  <label
                    className="block uppercase tracking-wide text-white text-xs font-bold mb-2"
                    htmlFor="grid-pages"
                  >
                    ISBN
                  </label>
                  <Field
                    className={`bg-gray-600  ${
                      errors.isbn && touched.isbn
                        ? "border-red-500 focus:border-red-500"
                        : "border-gray-200 focus:border-white"
                    } appearance-none border-2 rounded w-full py-2 px-4 text-white  leading-tight focus:outline-none placeholder-white `}
                    id="grid-isbn"
                    type="number"
                    placeholder="ISBN"
                    name="isbn"
                  />
                  <ErrorMessage
                    name="isbn"
                    component="p"
                    className="text-red-500 text-xs italic"
                  />
                </div>
              </div>
              <div className="flex flex-wrap mb-6">
                <div className="w-full px-3">
                  <label
                    className="block uppercase tracking-wide text-white text-xs font-bold mb-2"
                    htmlFor="grid-pages"
                  >
                    Author
                  </label>
                  <Field
                    className={`bg-gray-600  ${
                      errors.author && touched.author
                        ? "border-red-500 focus:border-red-500"
                        : "border-gray-200 focus:border-white"
                    } appearance-none border-2 rounded w-full py-2 px-4 text-white  leading-tight focus:outline-none placeholder-white `}
                    id="grid-author"
                    type="text"
                    placeholder="Author"
                    name="author"
                  />
                  <ErrorMessage
                    name="author"
                    component="p"
                    className="text-red-500 text-xs italic"
                  />
                </div>
              </div>
              <div className="flex flex-wrap mb-6">
                <div className="w-full px-3">
                  <label
                    className="block uppercase tracking-wide text-white text-xs font-bold mb-2"
                    htmlFor="grid-about"
                  >
                    File
                  </label>
                  <button
                    className="bg-orange-custom px-3 py-3 focus:outline-none rounded-md text-white"
                    onClick={() => document.getElementById("file").click()}
                  >
                    {values.file !== null
                      ? values.file.name
                      : "Attach Literature File"}
                  </button>
                  {/* <p>{values.file !== null ? values.file.name : ""}</p> */}
                  <input
                    className="hidden border-gray-200 appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white "
                    type="file"
                    placeholder="Attach Book File"
                    name="file"
                    id="file"
                    onChange={(e) =>
                      setFieldValue("file", e.currentTarget.files[0])
                    }
                  />
                  <ErrorMessage
                    name="file"
                    component="p"
                    className="text-red-500 text-xs italic"
                  />
                </div>
              </div>
              <div className="flex mb-6 justify-end">
                <div className="px-3">
                  <button
                    type="submit"
                    disabled={!(dirty && isValid)}
                    className={` ${
                      !(dirty && isValid)
                        ? "bg-gray-500 cursor-not-allowed"
                        : "bg-orange-custom hover:shadow-lg"
                    } focus:outline-none px-4 text-lg  py-2 text-white rounded-md flex items-center`}
                  >
                    Add Literature <BiBookAdd className="ml-3" />
                  </button>
                </div>
              </div>
            </Form>
          );
        }}
      </Formik>

      <Toast
        toggleToast={() => setshow(!show)}
        show={show}
        title={
          loginState.user.isAdmin === "0"
            ? "Thank you for adding your own literature to our website, please wait 1 x 24 hours to verify whether this book is your writing"
            : "Successfully Add literature"
        }
      />
    </div>
  );
}

export default AddBookForm;
