import { Transition } from "@tailwindui/react";
import React, { useContext, useEffect, useRef, useState } from "react";
import { NavLink, useHistory } from "react-router-dom";
import { BiBookAdd, BiLogOut } from "react-icons/bi";
import Logo from "./Logo";
import userImage from "../assets/image/men-user-default.png";
import { LoginContext } from "../context/loginContext";

function NavbarAdmin(props) {
  const [show, setshow] = useState(false);
  const history = useHistory();
  // eslint-disable-next-line
  const [state, dispatch] = useContext(LoginContext);
  const container = useRef(null);
  useEffect(() => {
    const handleOutsideClick = (event) => {
      if (!container.current.contains(event.target)) {
        if (!show) return;
        setshow(false);
      }
    };
    window.addEventListener("click", handleOutsideClick);
    return () => window.removeEventListener("click", handleOutsideClick);
  }, [show, container]);
  return (
    <div>
      <div className="flex flex-wrap flex-row justify-between px-10 py-6">
        <div>
          <Logo />
        </div>
        <div ref={container} className="relative inline-block text-left">
          <div
            onClick={() => setshow(!show)}
            className="cursor-pointer border-4 border-gray-600 shadow-xl mb-0 rounded-full mx-auto"
            style={imgStyle}
          ></div>
          <Transition
            show={show}
            enter="transition duration-100"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
          >
            <div className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg">
              <div
                className="rounded-md bg-white shadow-xs"
                role="menu"
                aria-orientation="vertical"
                aria-labelledby="options-menu"
              >
                <div className="py-1">
                  <NavLink
                    className="text-xl items-center font-bold flex px-3 py-4 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900"
                    role="menuitem"
                    to="/admin/add"
                  >
                    <BiBookAdd className="self-center text-2xl mr-2" />
                    Add Book
                  </NavLink>
                  <div className="border-t-4 border-gray-400"></div>
                  <button
                    className="w-full text-xl items-center font-bold text-left flex px-3 py-4 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900"
                    role="menuitem"
                    onClick={() => {
                      dispatch({
                        type: "LOGOUT",
                      });
                      history.push("/");
                    }}
                  >
                    <BiLogOut className="self-center text-2xl mr-2 text-red-600" />
                    Logout
                  </button>
                </div>
              </div>
            </div>
          </Transition>
        </div>
      </div>
    </div>
  );
}
const imgStyle = {
  backgroundImage: "url(" + userImage + ")",
  backgroundSize: "cover",
  backgroundPosition: "center",
  backgroundRepeat: "no-repeat",
  height: "50px",
  width: "50px",
};

export default NavbarAdmin;
