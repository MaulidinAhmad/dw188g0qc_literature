import React from "react";
import { useHistory } from "react-router-dom";
import { publicUrl } from "../config/api";
import { Document, Page, pdfjs } from "react-pdf";

function Literature(props) {
  pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
  const history = useHistory();
  return (
    <div>
      <div
        className={`cursor-pointer pl-auto text-white px-2 py-2  rounded-md ${
          props.active === "false" ? "bg-gray-300" : ""
        }`}
        style={props.active === "false" ? { backgroundColor: "#252525" } : {}}
        onClick={() => history.push(`/detail/${props.data.id}`)}
      >
        {props.active === "false" ? (
          <p className="text-center bold mb-2 text-red-500">
            Waiting To Be Verified
          </p>
        ) : (
          ""
        )}
        <div className="w-48 rounded-md" style={{ height: "17rem" }}>
          <Document
            className="mx-auto ml-2"
            file={`${publicUrl}/literature/${props.data.file}`}
          >
            <Page pageNumber={1} width={200} />
          </Document>
          {/* <img
            className="w-48 h-64 rounded-lg"
            src={`${publicUrl}/thumbnail/${props.data.thumbnailImg}`}
            alt=""
          /> */}
        </div>
        <div>
          <h1 className="text-title text-2xl leading-7 font-bold mt-4 mb-2">
            {props.data.title}
          </h1>
          <p className="text-gray-500">{props.data.author}</p>
        </div>
      </div>
    </div>
  );
}

export default Literature;
