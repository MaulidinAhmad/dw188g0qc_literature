import axios from "axios";

export const publicUrl = "http://localhost:5000";

export const baseBackendUrl = "http://localhost:5000/api/v1";
// Axios Default Url
export const API = axios.create({
  baseURL: baseBackendUrl,
});

// Set Auth Token
export const setAuthToken = (token) => {
  if (token) {
    API.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  } else {
    delete API.defaults.headers.common["Authorization"];
  }
};

export const headerConfig = {
  headers: {
    "Content-Type": "application/json",
  },
};
