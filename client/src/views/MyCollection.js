import React, { useEffect } from "react";
import Literature from "../components/Literature";
import Title from "../components/Title";
import { useQuery } from "react-query";
import { API } from "../config/api";
import ScreenLoader from "../components/Skeleton/ScreenLoader";
import NoData from "../components/Skeleton/NoData";
import { isEmptyArray } from "formik";

function Library(props) {
  const {
    isLoading,
    error,
    data: collectionData,
    refetch,
  } = useQuery("getCollection", () => API.get("/collections"));

  useEffect(() => {
    refetch();
  }, []);
  return (
    <div className="container">
      <div className="">
        {/* Library */}
        <Title name="My Collection" />
      </div>
      {/* card */}
      {isLoading || !collectionData ? (
        <ScreenLoader />
      ) : error ? (
        <h1>{error.message}</h1>
      ) : collectionData.data.data.length === 0 ? (
        <NoData />
      ) : (
        <>
          <div className="grid grid-cols-4 gap-0">
            {isEmptyArray(collectionData.data.data) ? (
              <NoData />
            ) : (
              collectionData.data.data.map((collection, index) => (
                <Literature key={index} data={collection.literature} />
              ))
            )}
          </div>
          )
        </>
      )}
    </div>
  );
}

export default Library;
