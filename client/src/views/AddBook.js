import React from "react";
import AddBookForm from "../components/Form/AddBookForm";
import Title from "../components/Title";

function AddBook(props) {
  return (
    <>
      <div>
        <Title name="Add Literature" />
      </div>
      <AddBookForm />
    </>
  );
}

export default AddBook;
