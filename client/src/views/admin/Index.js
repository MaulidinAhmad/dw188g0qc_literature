import React, { useState } from "react";
import { useMutation, useQuery } from "react-query";
import { FaCheckCircle } from "react-icons/fa";
import NavbarAdmin from "../../components/NavbarAdmin";
import Title from "../../components/Title";
import { API } from "../../config/api";

function Index(props) {
  // Status handle Function
  const statusHandle = (status) => {
    if (status === "Approved") {
      return <p className="text-success">Approve</p>;
    }
    if (status === null) {
      return <p className="text-yellow-600">Waiting to be verifed</p>;
    }
    if (status === "Canceled") {
      return <p className="text-danger">Cancel</p>;
    }
  };

  const [idLiterature, setidLiterature] = useState();
  // Use Query Get Literatures
  const {
    isLoading,
    error,
    data: literatureData,
    refetch,
  } = useQuery("getLiteratureAdmin", () => API.get("/literatures"));
  // Set Approve
  const [setApprove] = useMutation(async () => {
    try {
      await API.patch(`/literature/approve/${idLiterature}`);
      refetch();
    } catch (err) {
      console.log(err);
    }
  });
  // Set Cancel
  const [setCancel] = useMutation(async () => {
    try {
      const res = await API.patch(`/literature/cancel/${idLiterature}`);
      refetch();
    } catch (err) {
      console.log(err);
    }
  });
  // Handle Cancel Click
  const handleCancel = (id) => {
    setidLiterature(id);
    setCancel();
  };
  // Handle Approve Click
  const handleApprove = (id) => {
    setidLiterature(id);
    setApprove();
  };

  return (
    <div>
      <NavbarAdmin />
      <div className="bg-gray-100 min-h-screen px-16 py-12">
        <Title name="Literature verification" />
        <div>
          <table className="min-w-full divide-y divide-gray-400">
            <thead className="text-left">
              <tr className="bg-white">
                <th className="px-6 py-6">No</th>
                <th className="px-6 py-6">Users or Author</th>
                <th className="px-6 py-6">ISBN</th>
                <th className="px-6 py-6">E-literature</th>
                <th className="px-6 py-6">Status</th>
                <th className="px-6 py-6">Action</th>
              </tr>
            </thead>
            <tbody className="divide-y divide-gray-400 text-left">
              {isLoading || !literatureData?.data.data ? (
                <tr className="divide-y divide-gray-400 text-center">
                  <td className="px-6 py-6" colSpan="6">
                    <p>Loading....</p>
                  </td>
                </tr>
              ) : error ? (
                error.message
              ) : (
                literatureData.data.data.map((literature, index) => (
                  <tr key={index} className="divide-y divide-gray-400">
                    <td className="px-6 py-6">{index + 1}</td>
                    <td className="px-6 py-6">{literature.user.fullName}</td>
                    <td className="px-6 py-6">{literature.ISBN}</td>
                    <td className="px-6 py-6">{literature.file}</td>
                    <td className="px-6 py-6">
                      {statusHandle(literature.status)}
                    </td>
                    <td className="px-6 py-6">
                      {literature.status === "Canceled" ||
                      literature.status === null ? (
                        <div>
                          <button
                            onClick={() => handleCancel(literature.id)}
                            className="bg-danger py-1 text-white rounded-md px-3 mr-4"
                          >
                            Cancel
                          </button>
                          <button
                            onClick={() => handleApprove(literature.id)}
                            className="bg-success py-1 text-white rounded-md px-3"
                          >
                            Approve
                          </button>
                        </div>
                      ) : (
                        <>
                          <FaCheckCircle className="text-2xl text-success ml-6" />
                        </>
                      )}
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default Index;
