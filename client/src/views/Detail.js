import React, { useEffect, useState } from "react";
import { FaRegBookmark } from "react-icons/fa";
import { AiOutlineRight } from "react-icons/ai";
import { BsCloudDownload } from "react-icons/bs";
import { NavLink, useParams } from "react-router-dom";
import Toast from "../components/Toast/Toast";
import { useMutation, useQuery } from "react-query";
import { API, headerConfig, publicUrl } from "../config/api";
import ScreenLoader from "../components/Skeleton/ScreenLoader";
import { Document, Page, pdfjs } from "react-pdf";

function DetailBook(props) {
  pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
  const { id } = useParams();
  const [show, setshow] = useState(false);

  // eslint-disable-next-line
  const { isLoading, error, data: literature, refetch } = useQuery(
    "getDetail",
    () => API.get(`/literature/${id}`)
  );
  // Use Mutation For Add To Libarary
  const [addCollection] = useMutation(async () => {
    try {
      await API.post("/collection", { literatureId: id }, headerConfig);
    } catch (err) {
      console.log(err);
    }
  });
  // Getting Collection Data
  const {
    isLoading: collectionLoading,
    error: collectionError,
    data: collection,
    refetch: collectionRefetch,
  } = useQuery("getCollectionDetail", () =>
    API.get(`/collection?literatureId=${id}`)
  );
  // Use Mutation when remove collection
  const [removeCollection] = useMutation(async () => {
    try {
      const res = await API.delete(`/collection/${id}`);
    } catch (err) {
      console.log(err);
    }
  });
  // Use Effect If Show Toggler
  useEffect(() => {
    collectionRefetch();
  }, [show]);
  // Handler For Add collection
  const addCollectionHandler = () => {
    addCollection();
    collectionRefetch();
    setshow(!show);
  };
  // Handler For Remove collection
  const removeCollectionHandler = () => {
    removeCollection();
    collectionRefetch();
    setshow(!show);
  };
  // Download handler
  const downloadHandler = async (file) => {
    const res = await API.get("/literature/download/" + file);
    // var reader = new FileReader();
    // reader.readAsDataURL(res.data);
    // console.log(reader);
    // // console.log(res);
    // // const data = await res.data.toDataUrl();
    let url = publicUrl + "/literature/" + file;
    let a = document.createElement("a");
    a.href = url;
    a.target = "_blank";
    a.download = file;
    a.click();
  };
  return (
    <>
      {isLoading || !literature ? (
        <ScreenLoader />
      ) : error ? (
        <p> {error.message}</p>
      ) : (
        <>
          <div className="py-1 px-4 text-white">
            <div>
              <div className="grid md:grid-cols-12">
                <div className="col-span-4">
                  <Document
                    file={`${publicUrl}/literature/${literature.data.data.file}`}
                  >
                    <Page pageNumber={1} width={340} />
                  </Document>
                  {/* <img
                    className="mx-auto w-9/12 rounded-md"
                    style={{ height: "80%" }}
                    src={`${publicUrl}/thumbnail/${literature.data.data.thumbnailImg}`}
                    alt=""
                  /> */}
                </div>
                <div className="col-span-5">
                  {/* Top */}
                  <div>
                    <h1
                      className="text-6xl font-bold"
                      style={{ fontFamily: "Times New Roman" }}
                    >
                      {literature.data.data.title}
                    </h1>
                    <p className="text-2xl text-gray-600 mt-2">
                      {literature.data.data.author}
                    </p>
                  </div>
                  {/* Bot */}
                  <div className="mt-12">
                    <p className="text-2xl font-bold">Publication date</p>
                    <p className="text-gray-600 text-xl mb-8">
                      {literature.data.data.publication}
                    </p>
                    <p className="text-2xl font-bold">Pages</p>
                    <p className="text-gray-600 text-xl mb-8">
                      {literature.data.data.pages}
                    </p>
                    <p className="text-2xl font-bold text-orange-custom">
                      ISBN
                    </p>
                    <p className="text-gray-600 text-xl mb-8">
                      {literature.data.data.ISBN}
                    </p>
                    <div>
                      <button
                        onClick={() =>
                          downloadHandler(literature.data.data.file)
                        }
                        className="focus:outline-none bg-orange-custom text-white px-4 mr-4 py-3 rounded-md text-lg flex"
                      >
                        Download
                        <BsCloudDownload className="ml-2 place-self-center" />
                      </button>
                    </div>
                  </div>
                </div>
                <div className="col-span-3 justify-end pt-6 flex">
                  <div className="">
                    {collectionLoading || !collection ? (
                      collectionError ? (
                        ""
                      ) : (
                        ""
                      )
                    ) : !collection.data.data ? (
                      <button
                        onClick={() => addCollectionHandler()}
                        className="focus:outline-none bg-orange-custom text-white px-4 mr-4 py-3 rounded-md text-lg flex"
                      >
                        Add My collection
                        <FaRegBookmark className="ml-2 place-self-center" />
                      </button>
                    ) : (
                      <button
                        onClick={() => removeCollectionHandler()}
                        className="focus:outline-none bg-orange-custom text-white px-4 mr-4 py-3 rounded-md text-lg flex"
                      >
                        Remove collection
                        <FaRegBookmark className="ml-2 place-self-center" />
                      </button>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Toast
            toggleToast={() => setshow(!show)}
            show={show}
            title={
              collectionLoading || !collection
                ? ""
                : collection.data.data
                ? "Successfully remove literature"
                : "Your literature has been added successfully"
            }
          />
        </>
      )}
    </>
  );
}

export default DetailBook;
