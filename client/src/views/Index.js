import React, { useEffect, useState } from "react";
import { useQuery } from "react-query";
import IndexLogo from "../components/IndexLogo";
import Search from "../components/Search";
import NoData from "../components/Skeleton/NoData";
import ScreenLoader from "../components/Skeleton/ScreenLoader";
import { API } from "../config/api";
import Literature from "../components/Literature";
import SearchFilter from "../components/SearchFilter";

function Index(props) {
  const [showLogo, setshowLogo] = useState(true);
  const [name, setname] = useState();
  const [publication, setPublication] = useState();
  // eslint-disable-next-line
  const {
    isLoading,
    error,
    data: literatureData,
    refetch,
  } = useQuery("getFilterLiterature", () =>
    API.get(`/literatures?status=Approved&name=${name}&year=${publication}`)
  );

  useEffect(() => {
    refetch();
  }, [name, publication]);

  return (
    <div>
      <div
        className={`${
          showLogo ? "opacity-0" : "opacity-100"
        } flex flex-col transition duration-700 ease-out`}
      >
        <Search
          active={!showLogo}
          handleInput={(e) => setname(e)}
          handleShowLogo={() => ""}
          active={showLogo}
          initValue={name}
        />
      </div>
      <div
        className={`${
          showLogo ? "" : "hidden"
        } flex flex-col justify-center items-center`}
      >
        <IndexLogo show={showLogo} />
        <Search
          active={!showLogo}
          handleInput={(e) => setname(e)}
          handleShowLogo={() => {
            setshowLogo(!showLogo);
          }}
          initValue={name}
        />
      </div>
      {!showLogo ? (
        <div className="grid grid-cols-12 mt-12">
          <div className="col-span-2">
            <SearchFilter
              handleInput={(e) => {
                setPublication(e);
              }}
            />
          </div>
          {name === "" && publication === "" ? (
            ""
          ) : (
            <div className="col-span-10">
              {isLoading || !literatureData ? (
                <ScreenLoader />
              ) : error ? (
                <p>{error.message}</p>
              ) : literatureData.data.data.length === 0 ? (
                <div className="mt-10">
                  <NoData />
                </div>
              ) : (
                <div className="grid grid-cols-4 gap-10">
                  <>
                    {literatureData.data.data.map((literature, index) => (
                      <Literature
                        active={
                          literature.status === "Approved" ? "true" : "false"
                        }
                        key={index}
                        data={literature}
                      />
                    ))}
                  </>
                </div>
              )}
            </div>
          )}
        </div>
      ) : (
        <></>
      )}
    </div>
  );
}

export default Index;
