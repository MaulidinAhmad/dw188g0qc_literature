import React, { useContext, useState } from "react";
import { MdEmail, MdLocationOn, MdPhone } from "react-icons/md";
import { FaTransgender } from "react-icons/fa";
import Literature from "../components/Literature";
import Title from "../components/Title";
import ScreenLoader from "../components/Skeleton/ScreenLoader";
import { useQuery } from "react-query";
import { API, publicUrl } from "../config/api";
import { LoginContext } from "../context/loginContext";
import ProfilePictureModal from "../components/Modal/ProfilePictureModal";
import userImageMen from "../assets/image/men-user-default.png";
import userImageWomen from "../assets/image/women-user-default.png";
import NoData from "../components/Skeleton/NoData";

function Profile(props) {
  // eslint-disable-next-line
  const {
    isLoading,
    error,
    data: literatureData,
    refetch,
  } = useQuery("getUserLiterature", () => API.get("/userliteratures"));
  const [state] = useContext(LoginContext);
  const [show, setshow] = useState(false);
  return (
    <>
      <div>
        {/* container header */}
        <Title name="Profile" />
        <div
          className="w-full rounded-md mb-12 text-white"
          style={{ backgroundColor: "#252525" }}
        >
          <div className="grid grid-cols-12 px-10">
            {/* left col */}
            <div className="col-span-8 py-10">
              <div className="flex flex-col w-full">
                <div>
                  <div className="flex flex-row mb-6 w-full">
                    <div className="text-3xl mr-3 place-self-center text-orange-custom">
                      <MdEmail />
                    </div>
                    <div className="text-sm">
                      <p className="font-extrabold leading-4 ">
                        {state.user.email}
                      </p>
                      <p className="text-gray-600">Email</p>
                    </div>
                  </div>
                </div>
                <div>
                  <div className="flex flex-row mb-6 w-full">
                    <div className="text-3xl mr-3 place-self-center text-orange-custom">
                      <FaTransgender />
                    </div>
                    <div className="text-sm">
                      <p className="font-extrabold leading-4 ">
                        {state.user.gender}
                      </p>
                      <p className="text-gray-600">Gender</p>
                    </div>
                  </div>
                </div>
                <div>
                  <div className="flex flex-row mb-6 w-full">
                    <div className="text-3xl mr-3 place-self-center text-orange-custom">
                      <MdPhone />
                    </div>
                    <div className="text-sm">
                      <p className="font-extrabold leading-4 ">
                        {state.user.phone}
                      </p>
                      <p className="text-gray-600">Phone</p>
                    </div>
                  </div>
                </div>
                <div>
                  <div className="flex flex-row mb-6 w-full">
                    <div className="text-3xl mr-3 place-self-center text-orange-custom">
                      <MdLocationOn />
                    </div>
                    <div className="text-sm">
                      <p className="font-extrabold leading-4 ">
                        {state.user.address}
                      </p>
                      <p className="text-gray-600">Location</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* right col */}
            <div className="col-span-4 py-6">
              <div className="flex flex-col items-center">
                <div className="mb-4">
                  {!state.user.img ? (
                    <img
                      className="w-6/12 rounded-md mx-auto"
                      src={
                        state.user.gender === "male"
                          ? userImageMen
                          : userImageWomen
                      }
                      alt=""
                    />
                  ) : (
                    <img
                      className="rounded-md mx-auto"
                      src={publicUrl + "/thumbnail/user/" + state.user.img}
                      alt=""
                    />
                  )}
                </div>
                <div className="items-center">
                  <button
                    className="w-full px-4 py-2 bg-orange-custom focus:outline-none text-white rounded-md"
                    onClick={() => setshow(!show)}
                  >
                    Change Photo Profile
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div>
          {/* Library */}
          <div className="flex place-content-between">
            <Title name="My Literature" />
            <div className=""></div>
          </div>
          {/* card */}
          <div>
            {isLoading || !literatureData ? (
              <ScreenLoader />
            ) : error ? (
              <p>{error.message}</p>
            ) : literatureData.data.data.length === 0 ? (
              <NoData />
            ) : (
              <div className="grid grid-cols-5 gap-10">
                {literatureData.data.data.map((literature, index) => (
                  <Literature
                    active={literature.status === "Approved" ? "true" : "false"}
                    key={index}
                    data={literature}
                  />
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
      <ProfilePictureModal handleProfile={() => setshow(!show)} show={show} />
    </>
  );
}

export default Profile;
