import React, { useContext, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import Routes from "./routes";
import { LoginContext } from "./context/loginContext";
import { API, setAuthToken } from "./config/api";

if (localStorage.token) {
  setAuthToken(localStorage.token);
}

function App() {
  const [state, dispatch] = useContext(LoginContext);
  useEffect(() => {
    const loadUser = async () => {
      try {
        const res = await API.get("/auth");
        await dispatch({
          type: "USER_LOADED",
          payload: res.data.data.user,
        });
      } catch (err) {
        await dispatch({
          type: "AUTH_ERROR",
        });
      }
    };
    loadUser();
  }, [dispatch]);

  return (
    <div className="antialiased bg-black" style={{ fontFamily: "Nunito" }}>
      <BrowserRouter>
        <Routes />
      </BrowserRouter>
    </div>
  );
}

export default App;
